const log = console.log.bind(console);
const userIsLoggedIn = document.querySelector('.is-logged-in');
const userIsLoggedOut = document.querySelector('.is-logged-out');
const commentInput = document.querySelector('.comment-input');
const commentFooter = document.querySelector('.comment-footer');
const commentSubmit = document.querySelector('.comment-submit');
let localData = JSON.parse(localStorage.getItem('userEntity'));

// Add emojis
//new MeteorEmoji();

// Signing in
function onSignIn(googleUser) {
    var profile = googleUser.getBasicProfile();

    // Store the entity object in localStorage where it will be accessible from all pages
    let userEntity = {};
    userEntity.id = profile.getId();
    userEntity.name = profile.getName();
    userEntity.img = profile.getImageUrl();
    userEntity.email = profile.getEmail();
    localStorage.setItem('userEntity',JSON.stringify(userEntity));
    localData = JSON.parse(localStorage.getItem('userEntity'));

    userIsLoggedOut.style.display = 'none';
    userIsLoggedIn.style.display = 'flex';
    document.querySelector('.user-img').setAttribute('src', userEntity.img);
}

console.log(localData);
log(localStorage.getItem('userEntity'));

// Getting the users active status
function isLoggedIn() {
    return localStorage.getItem('userEntity') != null; // returns either true or false
}

// Logout
document.getElementById('l').addEventListener('click', () => {
    gapi.auth2.getAuthInstance().signOut();
    localStorage.clear();
    window.location.reload();
});

// User is either logged in or not
if (isLoggedIn()) {
    userIsLoggedIn.style.display = 'flex';
    document.querySelector('.user-img').setAttribute('src', localData.img);
}
else {
    userIsLoggedOut.style.display = 'block';
}

// Enable or disable the comment footer
window.addEventListener('mousedown', (e) => {
   if (!document.querySelector('.comment-enter-field').contains(e.target)) {
       commentInput.style.cssText = 'border-radius: 15px; height: 40px;';
       commentFooter.style.opacity = '0';
   }
   else {
       commentInput.style.cssText = 'border-radius: 15px 15px 0 0; height: 100px;';
       commentFooter.style.opacity = '1';
   }
});

// Typing a comment
commentInput.addEventListener('keyup', (e) => {
   if (e.target.value.length > 0) {
       commentSubmit.style.cssText = 'background: rebeccapurple; pointer-events: inherit';
   }
   else {
       commentSubmit.style.cssText = 'background: darkgrey; pointer-events: none;';
   }
});

// Submitting the comment

document.getElementById('comment-form').addEventListener('submit', (e) => {
    e.preventDefault();

    const commentData = new URLSearchParams();
    // Add comment to JSON
    localData.comment = commentInput.value;
    // Append all the data that we need
    commentData.append('data', JSON.stringify(localData));

    // Displaying the comment
    document.querySelector('.comment-input').value = '';
    document.querySelector('.comment-list').insertAdjacentHTML("afterBegin", `
        <div class="single-comment-area">
            <table>
                <tbody>
                    <tr>
                        <td>
                            <img alt="${localData.name}" class="profile-pic" src="${localData.img}">
                        </td>
                        <td>
                            <strong>${localData.name}</strong>
                            <div class="comment-field">
                                ${localData.comment}
                            </div>
                        </td>
                    </tr>
                </tbody>     
            </table>
        </div>
    `);

    fetch('../comments', {
        method: 'POST',
        body: commentData
    });
    log(commentData);
});
