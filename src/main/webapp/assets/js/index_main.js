let showCyberBoys = document.getElementById('showCyberboys');
let showSackmesser = document.getElementById('showSackmesser');
let backToHomePage = document.getElementById('backToHomePage');
let backToHomePage2 = document.getElementById('backToHomePage2');
let homePageIndex = document.getElementById('home-page-index');

(function($) {
    $(function() {
        // Navigation Toggle
        $('#nav-toggle').click(function() {
            $('nav ul').slideToggle();
        });
        // Hamburger Menu zu X toggeln
        $('#nav-toggle').on('click', function() {
            this.classList.toggle('active');
        });
    });
})(jQuery);

//Scroll nach oben
$(function() { $("#scroll-top").on('click', function() { $("HTML, BODY").animate({ scrollTop: 0 }, 1000); }); });

//Scroll-up erscheint nach dem scrollen
$(window).scroll(function(){
    if ($(this).scrollTop() > 500) {
        $('#scroll-top').fadeIn();
    } else {
        $('#scroll-top').fadeOut();
    }
});

jQuery('a[href*="#"]').on('click', function(e) {
    e.preventDefault()

    jQuery('html, body').animate(
        {
            scrollTop: jQuery(jQuery(this).attr('href')).offset().top,
        },
        500,
        'linear'
    )
});