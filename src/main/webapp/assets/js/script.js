const log = console.log.bind(console);

/* CHAT */
const webSocket = new WebSocket('ws://localhost:8080/test_war_exploded/chat');
const chatResult = document.querySelector('.result');
const loginArea = document.querySelector('.login-area');

// Add emojis
new MeteorEmoji();

// Open
webSocket.onopen = (msg) => {
    log("Server is now connected\n");
};

// Message
webSocket.onmessage = (msg) => {
    let data = (msg.data) ? JSON.parse(msg.data) : null;
    let formNickname = document.getElementById('form-nickname');
    // There is an error
    if (data.loginError !== 'null') {
        if (!document.querySelector('.error')) {
            formNickname.insertAdjacentHTML('beforeend',
                `<div class="error">
                        <strong>Error: </strong>
                        <span class="err-msg">${data.loginError}</span>
                   </div>`);
        }
        else {
            document.querySelector('.error .err-msg').innerHTML = data.loginError;
        }
    }
    else {
        // Template of adding a user
        let addUser = (nickname) => {
            let nick = nickname.trim();
            document.querySelector('.user-list').insertAdjacentHTML('beforeend', `
                <div class="user-section" data-user="${nick}">
                    <div class="random-user">
                        <i class="fa fa-user"></i>
                    </div>
                    <div class="random-nickname">
                        <i class="status online"></i>${nick}
                    </div>
                </div>
            `);
        };

        // Changing the status of a member
        let changeStatus = (newStatus, nickname) => {
            let status = document.querySelector(`.user-section .profile-pic[alt=${nickname.charAt(0).toUpperCase() + nickname.slice(1)}] + i`);
            if (status) {
                let oldStatus = (newStatus === 'online') ? 'offline' : 'online';
                status.classList.remove(oldStatus);
                status.classList.add(newStatus);
            }
        };

        // Add the new coming user to user list
        if (data.nickname !== "null" && data.eventType === "nickname") {
            // Set the status
            if (data.member === 'true') {
                changeStatus('online', data.nickname.toLowerCase());
            }
            else {
                addUser(data.nickname);
            }
            chatResult.insertAdjacentHTML("beforeend", `<span class="admin-msg green">* [${data.timestamp}] <strong>${data.nickname}</strong> has just joined</span>`);
        }

        // User has quit
        if (data.eventType === "quit" && data.nickname) {
            let userSection = document.querySelector(`.user-section[data-user=${data.nickname}]`);
            if (userSection) {
                userSection.remove();
            }
            else {
                let status = document.querySelector(`.user-section .profile-pic[alt=${data.nickname.charAt(0).toUpperCase() + data.nickname.slice(1)}] + i`);
                status.classList.remove("online");
                status.classList.add("offline");
            }
            chatResult.insertAdjacentHTML("beforeend", `<span class="admin-msg red">* [${data.timestamp}] <strong>${data.nickname}</strong> has just quit</span>`);
        }

        // User is logged in
        if (getComputedStyle(document.querySelector('.whois'), null).display !== 'none') {
            document.querySelector('.chat').style.height = 'inherit';
            document.querySelector('.whois').style.display = 'none';
            document.querySelector('.peer2peer').style.display = 'block';
            document.querySelector('.client-msg').focus();

            // Add all users to the user list for the user who joined the chat
            for (let user of data.users) {
                if (user !== data.nickname) {
                    // Add the normal user
                    let members = data.members.map((x) => x.toLowerCase());
                    if (!members.includes(user.toLowerCase())) {
                        addUser(user);
                    }
                    // Add a member
                    else {
                        changeStatus('online', user.toLowerCase());
                    }
                }
            }

        }
        // Login
        else {
            // Display the posted message
            if (data.message !== 'null' && data.message.trim() !== "") {
                let result = document.querySelector('.result');
                let profileImage = (data.member === 'null') ? '<i class="fa fa-user"></i>' : `<img class="profile-pic" src="../assets/img/${data.nickname}.png" alt="${data.nickname}">`;
                chatResult.innerHTML += `
                <div class="single-post">
                    <table>
                        <tr>
                            <td>
                                <div class="profile-pic">${profileImage.toLowerCase()}</div>  
                            </td>
                            <td>
                                <p class="personal-data">
                                    <strong>${data.nickname}</strong>, <span class="timestamp">${data.timestamp}</span>
                                </p>
                                <div class="personal-message">${data.message.replace(/</ig, '&lt;')}</div>
                            </td>
                        </tr>
                    </table>                                                                                        
                `;
                // Scroll to bottom
                result.scrollTop = result.scrollHeight;
            }
        }
    }
};

// Close
webSocket.onclose = () => { };

// Error
webSocket.onerror = () => {
  log("There is an error. Try it again later.");
};

// The user is choosing a nickname
document.getElementById("form-nickname").addEventListener('submit', (e) => {
    e.preventDefault();
    let data = {
        "nickname": `${document.getElementById('nickname').value}`
    };
    data = JSON.stringify(data);
    webSocket.send(data);
});

// User is member
document.getElementById('member').addEventListener('change', (event) => {
    // Show and hide the login area
    if (event.target.checked) {
        document.getElementById('form-nickname').style.right = '100vw';
        loginArea.style.left = 'calc(50% - 120px)';
        setTimeout(() => {
            document.getElementById('username').focus();
        }, 500)


    }
    else {
        document.getElementById('form-nickname').style.right = 'calc(50% - 120px)';
        loginArea.style.left = '100vw';
        document.getElementById('nickname').focus();
    }
});

// Posting a message in chat
document.querySelector(".peer2peer").addEventListener('submit', (e) => {
    e.preventDefault();
    let clientMsg = document.querySelector('.client-msg');
    let data = {
        "message": clientMsg.value,
    };
    data = JSON.stringify(data);
    webSocket.send(data);
    clientMsg.value = '';
});

/* ### USER MENU ### */
const userMenu = document.querySelector('.user-menu');
const userList = document.querySelector('.user-list');

// Opening user menu on smartphones
userMenu.addEventListener('click', () => {
    userMenu.style.display = 'none';
    userList.style.display = 'block';
});

// Closing the user menu on smartphones
document.querySelector('.user-list-close').addEventListener('click', () => {
    userList.style.display = 'none';
    userMenu.style.display = 'block';
});
/* ### */

// Nickname / login base page
if (getComputedStyle(document.querySelector('.whois'), null).display === 'block') {
    document.getElementById("nickname").focus();
}

/* ### LOGIN ### */
const login = document.getElementById('login');
if (login) {
    login.addEventListener('submit', (e) => {
        e.preventDefault();

        const loginData = new URLSearchParams();
        this.username = document.getElementById("username").value;
        this.password = document.getElementById("password").value;
        loginData.append('username', this.username);
        loginData.append('password', this.password);

        fetch('../login', {
            method: 'POST',
            body: loginData
        })
        .then((response) => {
            return response.text();
        })
        .then((data) => {

            // Access granted
            if (data === 'Access granted') {
                // Get the username with the login
                let data = {
                    "nickname": `${document.getElementById('username').value}`,
                    "member" : "true"
                };
                data = JSON.stringify(data);
                webSocket.send(data);

                //document.querySelector('.whois').style.display = 'none';
                //document.querySelector('.peer2peer').style.display = 'block';
            }
            // Access denied
            else {
                // Display the error
                if (!document.querySelector('.err-login')) {
                    document.getElementById('login').insertAdjacentHTML('beforeend',
                        `<div class="err-login">
                        <strong>Access denied!</strong>
                        Please correct your username or your password.
                   </div>`);
                }
            }
        })
        .catch(err => {
            console.error(err);
        })
    });
}