<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Chat</title>
    <link rel="stylesheet" href="../assets/css/style.css">
    <link href="https://fonts.googleapis.com/css?family=Roboto&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.8.2/css/all.min.css">
</head>
<body>

<div class="chat">
    <div class="whois">
        <h2>W&auml;hle einen Nickname</h2>

        <label for="member">
            <input id="member" type="checkbox">Ich bin ein Mitglied
        </label>

        <!-- Client's Nickname -->
        <form id="form-nickname">
            <input type="text" id="nickname" placeholder="Nickname">
            <input type="submit" value="Chat beitretten" class="nick-submit">
        </form>

        <!-- Member Login -->
        <div class="login-area">
            <form id="login">
                <label for="username">
                    <input id="username" name="username" placeholder="Username" type="text" autocomplete="username" required>
                </label>
                <label for="password">
                    <input id="password" name="password" placeholder="Password" type="password" autocomplete="password" required>
                </label>
                <input type="submit" value="Chat beitretten" class="nick-submit">
            </form>
        </div>
    </div>

    <!-- Official chat -->
    <form class="peer2peer">
        <div class="result"></div>
       <div class="user-menu mobile">
          <span class="fa fa-user"></span>
       </div>

        <div class="user-list">
           <div class="user-list-close mobile">x</div>
            <div class="user-section">
                <img class="profile-pic" src="../assets/img/chin.png" alt="Chin"> <i class="status offline"></i>Chin
            </div>
            <div class="user-section">
                <img class="profile-pic" src="../assets/img/jessica.png" alt="Jessica"> <i class="status offline"></i>Jessica
            </div>
            <div class="user-section">
                <img class="profile-pic" src="../assets/img/kevin.png" alt="Kevin"> <i class="status offline"></i>Kevin
            </div>
            <div class="user-section">
                <img class="profile-pic" src="../assets/img/nicolas.png" alt="Nicolas"> <i class="status offline"></i>Nicolas
            </div>
            <div class="user-section">
                <img class="profile-pic" src="../assets/img/philipp.png" alt="Philipp"> <i class="status offline"></i>Philipp
            </div>
            <div class="user-section">
                <img class="profile-pic" src="../assets/img/raphael.png" alt="Raphael"> <i class="status offline"></i>Raphael
            </div>
            <div class="user-section">
                <img class="profile-pic" src="../assets/img/reza.png" alt="Reza"> <i class="status offline"></i>Reza
            </div>
            <div class="user-section">
                <img class="profile-pic" src="../assets/img/sandro.png" alt="Sandro"> <i class="status offline"></i>Sandro
            </div>
            <div class="user-section">
                <img class="profile-pic" src="../assets/img/tim.png" alt="Tim"> <i class="status offline"></i>Tim
            </div>
        </div>
        <div class="msg-area">
            <input data-meteor-emoji="true" type="text" class="client-msg" placeholder="What's up?">
           <button class="submit-msg" type="submit"><i class="fas fa-paper-plane"></i></button>
        </div>
    </form>
</div>

<script src="../assets/js/plugins/meteorEmoji.min.js"></script>
<script src='../assets/js/script.js'></script>
</body>
</html>