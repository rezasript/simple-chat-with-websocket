<%@ page import="ch.yourclick.zt.commentArea.Comments" %>
<%@ page import="ch.yourclick.zt.Database" %>
<%@ page import="ch.yourclick.zt.Row" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.Arrays" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
   <title>OAuth</title>
   <meta name="google-signin-client_id" content="420430821131-c5t2p83bvsca290e6iu4h1ih5t1bpg45.apps.googleusercontent.com">
   <link rel="stylesheet" href="../assets/css/style.css">
   <script src="https://apis.google.com/js/platform.js?onload=init" defer async></script>
</head>
<body>
<div class="comment-area">
   <div class="is-logged-out hidden">
      <p class="login-info">To write a message, please login with Google.</p>
      <div class="g-signin2" data-onsuccess="onSignIn"></div>
   </div>
   <div class="is-logged-in hidden">
      <img src="" alt="" class="user-img">
      <form action="#" id="comment-form">
         <div class="comment-enter-field">
               <textarea placeholder="Write a comment..." class="comment-input" name="comment-input"></textarea>
               <div class="comment-footer">
                  <input type="submit" value="Comment" class="comment-submit">
               </div>
         </div>
      </form>
   </div>
   <div class="comment-list">
      <%
         List<String> columns = Arrays.asList("comment", "name", "image");
         Database database = new Database().getAll("SELECT * FROM zt_productions.comments ORDER BY timestamp DESC", columns);
         for (Row db : database.sqlData) {
      %>
         <div class="single-comment-area">
            <table>
               <tr>
                  <td>
                     <img class="profile-pic" src="<%= db.get("image") %>">
                  </td>
                  <td>
                     <strong><%= db.get("name") %></strong>
                     <div class="comment-field">
                        <%= db.get("comment") %>
                     </div>
                  </td>
               </tr>
            </table>
         </div>
      <% } %>

   </div>
</div>
<a href="#" id="l">logout</a>
<script src="../assets/js/oauth.js"></script>
</body>
</html>