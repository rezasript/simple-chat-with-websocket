<%@ page import="ch.yourclick.zt.Login" %><%--
  Created by IntelliJ IDEA.
  User: black
  Date: 02.05.2019
  Time: 23:34
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html lang="de">
<head>
   <title>ZT-Productions</title>
   <meta http-equiv="content-type" content="text/html; charset=utf-8" />
   <meta name="viewport" content="width=device-width, user-scalable=no, minimum-scale=1.0, maximum-scale=1.0">
   <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700,900&display=swap" rel="stylesheet">
   <link rel="icon" type="image/png" sizes="16x16" href="assets/img/favicon-16x16.png">
   <link rel="stylesheet" type="text/css" href="assets/css/style.css">
</head>
<body>

<div id="header">
   <nav>
      <a href="#header"><img class="logo-zt" src="assets/img/Logo_ZT-Productions.png" alt="zt-productions-logo"></a>
      <div id="nav-toggle"><span></span></div><!--end nav-toggle-->
      <div id="container-menu">
         <ul class="nav-list">
            <li><a href="#demoprodukt">3D Demonstration</a></li>
            <li><a href="#service">Service</a></li>
            <li><a href="#mission">Mission</a></li>
            <li><a href="#partner">Partners</a></li>
            <li><a href="#team">Team</a></li>
         </ul>
      </div><!--end container-menu-->
   </nav>
</div><!--end header-->

<div id="showbox">
   <div class="main_container">
      <div id="main">
         <h1>Die All-In-One Lösung für 3D Web Integrationen</h1>
         <button class="main_button"><a href="#demoprodukt">Technologie</a></button>
      </div><!--end main-->
   </div><!--end main_container-->
</div><!--end showbox-->

<div id="demoprodukt"></div>
<div id="container-linking">
   <div class="linking_left">
      <div class="overlay">
         <h3>Stellen Sie ihr Unternehmen ins Rampenlicht</h3>
         <p>Mit der interaktiven Teamvorstellung kann Ihr Kunde auf moderne sowie humorvolle Art und Weise Ihr Team kennenlernen.</p>
         <a id="showCyberboys" href="cyberboys/cyberboys.jsp"><button class="main_button">zur Demo</button></a>
      </div><!--end overlay-->
   </div><!--end linking_left-->

   <div class="linking_right">
      <div class="overlay">
         <h3>Zeigen Sie Ihr Produkt in voller Pracht</h3>
         <p>In der Produktvorstellung kann Ihr Kunde auf eine spielerische Art und Weise Ihr Produkt begutachten
            und sich darüber informieren.</p>
         <a id="showSackmesser" href="sackmesser/sackmesser.html"><button class="main_button">zur Demo</button></a>
      </div><!--end overlay-->
   </div><!--end linking_right-->
   <div style="clear:both;"></div>
</div><!--end container-linking-->

<div id="content">
   <div id="service" class="anker"></div>
   <h2>Service</h2>
   <div class="our_service">
      <div class="service_container">
         <img class="service_img" src="assets/img/our_service_3d.png" alt="our_service_png">
         <h5>3D Production Pipeline</h5>
      </div><!--end service_container1-->
      <div class="service_container">
         <img class="service_img" src="assets/img/our_service_webgl.png" alt="our_service_png">
         <h5>WebGL Integration</h5>
      </div><!--end service_container2-->
      <div class="service_container">
         <img class="service_img" src="assets/img/our_service_website.png" alt="our_service_png">
         <h5>Full Website Production</h5>
      </div><!--end service_container3-->
   </div><!--end our_service-->
   <p class="easy_text">
      Für Leute ohne Fachkenntnisse ist es schwierig und aufwendig, eine 3D basierte Webseite zu produzieren,
      die eine ansprechende und benutzerfreundliche Erfahrung bietet. </p>
   <p class="easy_text">Wir von ZT-Productions bieten eine Komplettlösung
      von der Skizze bis zur fertigen Integration nach Ihrem Wunsch. Durch die einfach zugängliche Interaktivität stechen
      Sie aus der Menge heraus und schaffen einen persönlichen Bezug zu Ihrem Kunden, und das zu einem bezahlbaren Preis.
   </p>

   <div id="mission" class="anker"> </div>
   <h2>Mission</h2>
   <p class="easy_text">
      Mit einem webbasierten 3D-Produkt werden wir den Betrachter animieren, sich durch
      Interaktionen mit dem Unternehmen oder Produkt auseinanderzusetzen.
   </p>
   <p class="easy_text">
      Wir bieten die All-in-One Lösung für 3D Webintegrationen.
   </p>

   <div id="partner" class="anker"></div>
   <div class="our_partners">
      <h2>Partners</h2>
      <div class="partner_container">
         <img src="assets/img/partner1.png" alt="our_partner_png">
         <img src="assets/img/partner3.png" alt="our_partner_png">
         <img src="assets/img/partner4.png" alt="our_partner_png">
         <img src="assets/img/partner5.png" alt="our_partner_png">
      </div><!--end partner_container-->
   </div><!--end our_partners-->

   <div id="team" class="anker"></div>
   <h2>Team</h2>
   <p class="easy_text">
      Wir sind ein junges und motiviertes Team, welches zurzeit aus neun Mitgliedern besteht.
   </p>
   <p class="easy_text">
      Zum grössten Teil besteht unser Team aus 3D-Artists, die 3D-Objekte vom Konzept bis zum finalen Export produzieren.
      Des weiteren haben wir auch noch Web-Entwickler, die für Programmierung sowie Web-Implementation verantwortlich sind.
   </p>
</div><!--end content-->

<footer>
   <div class="footer_description">
      <img class="logo_footer" src="assets/img/Logo_ZT-Productions.png" alt="zt-productions-logo">
      <p>
         Für Leute ohne Fachkenntnisse ist es schwierig und aufwendig, eine 3D basierte Webseite zu produzieren,
         die eine ansprechende und benutzerfreundliche Erfahrung bietet. Wir von ZT-Productions bieten eine Komplettlösung
         von der Skizze bis zur fertigen Integration nach Ihrem Wunsch. Durch die einfach zugängliche Interaktivität stechen
         Sie aus der Menge heraus und schaffen einen persönlichen Bezug zu Ihrem Kunden, und das zu einem bezahlbaren Preis.
      </p>
      <p><a class="email-info" href="mailto:info@yourclick.ch?subject=feedback">info@yourclick.ch</a></p>
   </div><!--end footer_description-->
   <div class="footer_links">
      <h2>Links</h2>
      <p><a href="#demoprodukt">Produkte</a></p>
      <p><a href="#service">Service</a></p>
      <p><a href="#mission">Mission</a></p>
      <p><a href="#partner">Partners</a></p>
      <p><a href="#team">Team</a></p>
   </div><!--end footer_description-->
   <div style="clear:both;"></div>
   <div class="footer_copyright">
      <p>© 2019 ZT-Productions All right reserved</p>
   </div><!--end footer_copyright-->
   <div style="clear:both;"></div>
</footer>

<a id="scroll-top" href="#header"><img src="assets/img/scroll-up.png" alt="scroll-up"></a>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="assets/js/index_main.js"></script>

</body>
</html>