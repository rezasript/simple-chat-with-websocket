package ch.yourclick.zt.commentArea;

import ch.yourclick.zt.Database;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;

@WebServlet("/comments")
public class Comments extends HttpServlet {
    private Database database = new Database();

    @Override
    public void doPost(HttpServletRequest req, HttpServletResponse resp) {

        System.out.println(req.getParameter("data"));

        JsonParser jsonParser = new JsonParser();
        JsonObject json = jsonParser.parse(req.getParameter("data")).getAsJsonObject();

        // Values
        ArrayList<JsonElement> values = new ArrayList<>();
        values.add(json.get("name"));
        values.add(json.get("email"));
        values.add(json.get("img"));
        values.add(json.get("comment"));
        String valuesAsString = values.toString().replaceAll("[\\[\\]]", "");

        database.add("name, email, image, comment", valuesAsString);
    }

}