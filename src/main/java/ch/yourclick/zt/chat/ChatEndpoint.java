package ch.yourclick.zt.chat;

import ch.yourclick.zt.Database;
import ch.yourclick.zt.Helper;
import com.google.gson.Gson;
import javax.websocket.*;
import javax.websocket.server.ServerEndpoint;
import java.io.IOException;
import java.util.*;

@ServerEndpoint("/chat")
public class ChatEndpoint {
    private HashMap<String, String> user;
    private String currentTime;
    private Gson gson = new Gson();
    private Database database = new Database();
    private static final Set<Session> users = Collections.synchronizedSet(new HashSet<>());

    @OnOpen
    public void handleOpen() {

    }

    @OnMessage
    public void handleMessage(String message, Session userSession) throws Exception {
        postMessage();
        Data json = gson.fromJson(message, Data.class);

        /* eventType: nickname */
        // Get the username
        if (userSession.getUserProperties().get("username") == null) {
            // Create a nickname list
            ArrayList<String> nicknameList = new ArrayList<>();
            for (Session user : users) {
                nicknameList.add((String) user.getUserProperties().get("username"));
            }

            /* THROW ERRORS */
            // Nickname is too long
            if (json.nickname.length() > 30) {
                Data data = new Data("nickname", "null", "null", "null", "null", new ArrayList<>(), new ArrayList<>(), "Nickname is too long");
                userSession.getBasicRemote().sendText(gson.toJson(data));
                return;
            }
            // Nickname is taken
            List<String> columns = Arrays.asList("username");
            Database username = database.getAll("SELECT username FROM users WHERE username = '" + json.nickname + "'", columns);
            if ((nicknameList.stream().anyMatch(message::equalsIgnoreCase)) || (!username.getSingleField("username").isEmpty()) && json.member == null) {
                Data data = new Data("nickname", "null", "null", "null", "null", new ArrayList<>(), new ArrayList<>(), "Nickname is already taken");
                userSession.getBasicRemote().sendText(gson.toJson(data));
            }

            else {
                // User is good, let him in
                userSession.getUserProperties().put("username", json.nickname);
                users.add(userSession);

                // Get all members
                List<String> usernameColumn = Arrays.asList("username");
                ArrayList<String> members = database.getAll("SELECT username FROM users", usernameColumn).getSingleField("username");

                // Send the data
                Database correctUser = database.getAll("SELECT * FROM users WHERE username = '" + userSession.getUserProperties().get("username") + "'", usernameColumn);
                Data data = new Data("nickname", currentTime, (String) userSession.getUserProperties().get("username"), "null", "null", members, nicknameList, "null");
                if (!correctUser.getSingleField("username").isEmpty()) {
                    data = new Data("nickname", currentTime, (String) userSession.getUserProperties().get("username"), "null", "true", members, nicknameList, "null");
                }

                for (Session user : users) {
                    user.getBasicRemote().sendText(gson.toJson(data));
                }
            }
        }
        else {
            /* eventType: message */

            // Post the message
            List<String> usernameColumn = Arrays.asList("username");
            ArrayList<String> members = database.getAll("SELECT username FROM users", usernameColumn).getSingleField("username");
            for (Session user : users) {

                List<String> columns = Arrays.asList("username");
                Database allData = database.getAll("SELECT * FROM users WHERE username = '" + userSession.getUserProperties().get("username") + "'", columns);

                Data data = new Data("message", this.currentTime, (String) userSession.getUserProperties().get("username"), json.message, "true", members, new ArrayList<>(),"null");
                if (allData.getSingleField("username").isEmpty()) {
                    data = new Data("message", this.currentTime, (String) userSession.getUserProperties().get("username"), json.message, "null", members, new ArrayList<>(), "null");
                }
                user.getBasicRemote().sendText(gson.toJson(data));
            }
        }
    }

@OnClose
public void handleClose(Session userSession) {
    synchronized (users) {
        users.remove(userSession);
        // Post the message
        for (Session user : users) {
            postMessage();

            List<String> columns = Arrays.asList("username");
            Database allData = database.getAll("SELECT * FROM users WHERE username = '" + userSession.getUserProperties().get("username") + "'", columns);
            Data data = new Data("quit", currentTime, (String) userSession.getUserProperties().get("username"), "null", "null", new ArrayList<>(), new ArrayList<>(), "null");
            if (!allData.getSingleField("username").isEmpty()) {
                data = new Data("quit", this.currentTime, (String) userSession.getUserProperties().get("username"), "null", "true", new ArrayList<>(), new ArrayList<>(), "null");
            }
            try {
                if (user.isOpen()) {
                    user.getBasicRemote().sendText(gson.toJson(data));
                }
            }
            catch (IOException e) {
                System.out.println(e.getLocalizedMessage());
            }
        }
    }
}

    @OnError
    public void handleError(Throwable t) {
        t.printStackTrace();
    }

    private void postMessage() {
        Helper helper = new Helper();
        this.currentTime = helper.getTheDate("HH:mm:ss");

        HashMap<String, String> user = new HashMap<>();
        user.put("string", "username");
        this.user = user;
    }
}
