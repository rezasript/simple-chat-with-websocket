package ch.yourclick.zt.chat;

import ch.yourclick.zt.Database;
import ch.yourclick.zt.Row;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

class Data {
    public String nickname;
    public String message;
    public String member;
    private String eventType;
    private String timestamp;
    private ArrayList<String> members;
    private ArrayList<String> users;
    private String loginError;

    Data(String eventType, String timestamp, String nickname, String message, String member, ArrayList<String> members, ArrayList<String> users, String loginError) {
        this.eventType = eventType;
        this.timestamp = timestamp;
        this.nickname = nickname;
        this.message = message;
        this.member = member;
        this.members = members;
        this.users = users;
        this.loginError = loginError;
    }
}
