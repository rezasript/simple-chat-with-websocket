package ch.yourclick.zt;

import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.Map;

/**
 * Helper will help you with your code
 */
public class Helper {
    /**
     *
     * @param parameters
     * This method is as simple as PHPs var_dump() function
     */
    public static void varDump(Map<String, String[]> parameters) {
       parameters.entrySet().stream().map(e -> e.getKey() + " -> " + Arrays.toString(e.getValue())).forEach(System.out::println);
    }

    /**
     * Get the current date
     * @param format (e.g. "HH:mm:ss")
     * @return the current date as string
     */
    public String getTheDate(String format) {
        SimpleDateFormat dateFormatter = new SimpleDateFormat(format);
        Date date = new Date();
        return dateFormatter.format(date);
    }
}