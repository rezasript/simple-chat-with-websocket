package ch.yourclick.zt;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.*;

@WebServlet("/login")
public class Login extends HttpServlet {
    @Override
    public void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {

        PrintWriter output = resp.getWriter();
        HttpSession session = req.getSession();

        req.setAttribute("username", req.getParameter("username"));
        req.setAttribute("password", req.getParameter("password"));

        Map<String, String> loginData = new HashMap<>();
        loginData.put("string", "username");

        // Let's debug
        //Helper.varDump(req.getParameterMap());

        // User is logged in
        Database database = new Database();

        List<String> columns = Arrays.asList("username", "password");
        Database allData = database.getAll("SELECT * FROM users WHERE username = '" + req.getParameter("username") + "' AND password = '" + req.getParameter("password") + "'", columns);

        if (!allData.getSingleField("username").isEmpty()) {
            session.setAttribute("user", req.getParameter("username"));
            output.write("Access granted");
            System.out.println(session.getAttribute("user"));
        }
        // Access denied
        else {
            output.write("Access denied");
        }
    }
}