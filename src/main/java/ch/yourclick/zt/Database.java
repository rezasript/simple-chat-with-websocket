package ch.yourclick.zt;

import java.sql.*;
import java.util.*;

public class Database {
    private String url = "jdbc:mariadb://localhost:3306/zt_productions?user=root&password=test";
    public List<Row> sqlData;
    /**
     *
     * @param sql SQL string
     * @param columns Table column
     * @return Returns the result
     */
    public List<Row> getData(String sql, List<String> columns) {
        this.driver();

        Map<String, Integer> columnIndex = new HashMap<>();
        int ix = 0;
        for (String column : columns) {
            columnIndex.put(column, ix);
            ix++;
        }

        // try-with-resources, um die geoeffneten Objekte wieder zu schliessen!
        try(Connection conn = DriverManager.getConnection(this.url);
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql)) {

            List<Row> result = new ArrayList<>();

            while (rs.next()) {
                List<String> row = new ArrayList<>();
                for (String column : columns) {
                    row.add(rs.getString(column.replaceAll("[\\[\\]]", "")));
                }
                result.add(new Row(row, columnIndex));
            }

            return result;
        }
        catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public Database getAll(String sql, List<String> columns) {
        Database database = new Database();
        this.sqlData = database.getData(sql, columns);
        return this;
    }

    public ArrayList<String> getSingleField(String field) {
        //List<Row> result = getAll();

        ArrayList<String> column = new ArrayList<>();
        for (Row row : this.sqlData) {
            for (String columnName : row.getColumnNames()) {
                if (columnName.equals(field)) {
                    column.add(row.get(columnName));
                }
            }
        }
        return column;
    }

    public void add(String columns, String values) {
        this.driver();
        try {
            Connection conn = DriverManager.getConnection(this.url);
            Statement stmt = conn.createStatement();
            String sql = "INSERT INTO zt_productions.comments ("+ columns +") VALUES ("+ values +")";
            stmt.executeUpdate(sql);
        }
        catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Get the driver of MariaDB
     */
    private void driver() {
        // Ensure we have mariadb Driver in classpath
        try {
            Class.forName("org.mariadb.jdbc.Driver");
        }
        catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

}