package ch.yourclick.zt;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class Row {
    private final Map<String, Integer> columnIndex;
    private final List<String> row;

    public Row(List<String> row, Map<String, Integer> columnIndex) {
        this.row = row;
        this.columnIndex = columnIndex;
    }

    public Set<String> getColumnNames() {
        return Collections.unmodifiableSet(columnIndex.keySet());
    }

    public String get(String columnName) {
        String result = null;
        Integer index = columnIndex.get(columnName);
        if (index != null) {
            result = row.get(index);
        }
        return result;
    }
}